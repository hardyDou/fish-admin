# fish-admin

#### 介绍
fish-admin基于layui的轻量级后台管理的前端框架。针对于开发者来讲做了非常棒的优化。其中非常棒的功能是允许开发者在单个页面define多个模块，这样的好处是你可以将一个复杂页面拆分多个模块，在打包的时候再将其合并到一个文件。这样既满足了开发过程中的体验、也不影响模块散乱。

#### 演示地址
http://hardydou.gitee.io/fish-admin/

#### 特点
- 支持5级菜单、要支持更多级别只需要配置一些css即可
- 支持单个文件定义多个模块(即在一个页面上面可以define，元layui必须从文件对应文件加载不够方便)
- 对layiui完全兼容

#### 软件架构
基于layui 2.4.3版本；
使用bower管理依赖；
使用karma自动化测试；
使用gulp打包发布；

#### 使用说明

1.  引入 layui 的js、css
```
<link rel="stylesheet" href="./layui/dist/css/layui.css">
<script src="./layui/src/layui.js" charset="utf-8"></script>
```
2.  引入 fish-admin 的 js、css 即可；
```
<link rel="stylesheet" href="./css/fa.css">
<script src="./js/fa.js" charset="utf-8"></script>
<!-- 这里也可以引用all文件 -->
<script src="./js/fa.all.js" charset="utf-8"></script>
```
3.  用fa.use\define 替换layui.use\define
```
fa.define(['jquery'],(exports)=>{
    let $=layui.jquery;
    exports('fa_util',
        {
            info:(msg)=>{console.log(msg)}
        }
    )
});
fa.use(['fa_util'],()=>{
    let util=layui.fa_util;
    util.inf('hello word !');
})
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

