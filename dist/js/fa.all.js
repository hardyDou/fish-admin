/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

let Fa=function(){this.cache={};this.modules={};this.lisenter={};this.options={};}
const fa=new Fa();Fa.prototype.load=function(name,onLoad){if(this.cache[name]){onLoad(this.cache[name]);}
if(this.modules[name]){this.lisenter[name]=this.lisenter[name]||[];this.lisenter[name].push((mod)=>{onLoad(mod);});}}
Fa.prototype.set=function(name,app){this.cache[name]=app;(this.lisenter[name]||[]).forEach(onLoad=>onLoad(app));}
Fa.prototype.inner=function(name){this.modules[name]=true;}
Fa.prototype.unSupport=function(name){return!(this.cache[name]||this.modules[name]);}
Fa.prototype.use=function(deps,callback){if(typeof deps=="function"){callback=deps;deps=[];}
deps=(typeof deps==='string')?[deps]:deps;let layuiDeps=deps.filter(dep=>this.unSupport(dep));let faDeps=deps.filter(dep=>!this.unSupport(dep));let count=faDeps.length;faDeps.forEach(dep=>{this.load(dep,(mod)=>{layui.dep=mod;count--;})});let tmp=setInterval(()=>{if(count<=0){clearInterval(tmp);layui.use(layuiDeps,callback);}},5)}
Fa.prototype.define=function(deps,callback){if(typeof deps=='function'){callback=deps;deps=[];}
deps=(typeof deps==='string')?[deps]:deps;let layuiDeps=deps.filter((dep)=>this.unSupport(dep));let faDeps=deps.filter((dep)=>!this.unSupport(dep));this.use(faDeps,()=>{layui.define(layuiDeps,(factory)=>{callback((app,exports)=>{this.set(app,exports);factory(app,exports);});});});}
Fa.prototype.config=function(options){layui.each(options,(k)=>{this.options[k]=options[k]});layui.config(options);};(()=>{this.mods=['fa_util','fa_event','sha','fa_setting','fa_head','fa_nav','fa_tab','fa_todolist','fa_intro']
this.mods.forEach(x=>(typeof fa=='object')&&fa.inner(x));})();fa.define(['fa_util','fa_setting'],function(exports){'use strict';let MOD_NAME="body_cls";let util=layui.fa_util;const listener=[window];let params={};function addListener(w){listener.push(w);util.toggleClass(w.document.body,params.oldVal,params.val);}
function change(np){params=np;listener.forEach(w=>{util.toggleClass(w.document.body,params.oldVal,params.val);})}
exports(MOD_NAME,{addListener:addListener,change:change});});;fa.define(['jquery'],function(exports){'use static';let $=layui.jquery;let FaEchart=function(){};FaEchart.prototype.echart=function(view,options){var myChart=echarts.init(document.querySelector(view));myChart.setOption(options);return myChart;}
const OPTS={line:function(opt={xTitle:[],series:[]}){let def={legend:{},grid:{left:'3%',right:'4%',bottom:'3%',containLabel:true},tooltip:{trigger:'axis'},xAxis:{type:'category',boundaryGap:true,data:opt.xTitle||[]},yAxis:{type:'value'},series:opt.series||[]};return def;},heatmap:function(opt={xTitle:[],yTitle:[],min:0,max:0,name:"",data:[],label:false}){let def={tooltip:{},grid:{left:'1%',right:'1%',top:'2%',bottom:'15%',containLabel:true},xAxis:{type:'category',data:opt.xTitle||[],splitArea:{show:true}},yAxis:{type:'category',data:opt.yTitle||[],splitArea:{show:true}},visualMap:{min:opt.min||0,max:opt.max||10,calculable:true,orient:'horizontal',left:'center',bottom:'0%',itemWidth:14},series:[{name:opt.name||"name",type:'heatmap',data:opt.data||[],label:{show:opt.label||true},emphasis:{itemStyle:{shadowBlur:10,shadowOffsetX:0,shadowColor:'rgba(0, 0, 0, 0.5)'}}}]}
return def;},pie:function(opt={name:"",radius:'',data:[]}){let def={tooltip:{trigger:'item'},series:[{name:opt.name||'',type:'pie',radius:opt.radius||'80%',data:opt.data||[],emphasis:{itemStyle:{shadowBlur:10,shadowOffsetX:0,shadowColor:'rgba(0, 0, 0, 0.5)'}}}]};return def;},bar:function(opt={xTitle:[],data:[]}){let def={legend:{},tooltip:{},grid:{left:'3%',right:'4%',bottom:'3%',containLabel:true},xAxis:{type:'category',data:opt.xTitle||[]},yAxis:{type:'value'},series:opt.data||[]};return def;},radar:function(opt={legend:[],name:"",data:[],indicator:[]}){let def={tooltip:{},legend:{data:opt.legend||[]},radar:{indicator:opt.indicator||[]},series:[{name:opt.name||'',type:'radar',data:opt.data||[]}]}
return def;}}
FaEchart.prototype.buildOpts=function(type,options={legend:{},xTitle:[],series:[]}){let opts=OPTS[type](options);if(opts.config){$.extend(opts,options.config);}
return opts;}
exports('fa_echart',new FaEchart());});fa.define(['fa_util'],function(exports){'use strict';let MOD_NAME='fa_event',util=layui.fa_util;util.config({debug:true,module:MOD_NAME});let Fa_Event=function(){this.init();}
Fa_Event.prototype.init=function(){this.EL={};}
Fa_Event.prototype.event=function(mod,event,params){let mods=["*",mod];let faEvent={mod:mod,event:event};mods.forEach(m=>{let key=this.genKey(m,event);this.EL[key]=this.EL[key]||[];this.EL[key].forEach(cb=>{cb&&(typeof cb==='function')&&cb.call(this,faEvent,params);});})}
Fa_Event.prototype.onEvent=function(mod,event,callback){let key=this.genKey(mod,event);this.EL[key]=this.EL[key]||[];this.EL[key].push(callback);}
Fa_Event.prototype.genKey=function(mod,event){return mod+"_@FA@_"+event;}
exports(MOD_NAME,{e:new Fa_Event(),event:function(mod,event,params){this.e.event(mod,event,params);},onEvent:function(mod,event,callback){this.e.onEvent(mod,event,callback);}});});;fa.define(['jquery','fa_util','fa_event','fa_setting'],function(exports){let $=layui.$,MOD_NAME="fa_head",FaEvent=layui.fa_event,faSetting=layui.fa_setting,util=layui.fa_util;util.config({debug:true,module:MOD_NAME});let Fa_Head=function(){this.o={box:'.fa-header',lockscreen:".fa-lock-box",sideSwitch:$('.fa-side-switch'),refreshBtn:$('.fa-refresh'),body:$('body'),side_off:'mini-side',hideCls:'fa-hide'}}
Fa_Head.prototype.init=function(){let _this=this;$("*[fa-event]").on('click',function(e){let event=$(e.currentTarget).attr('fa-event');event&&FaEvent.event(MOD_NAME,event,e);});FaEvent.onEvent(MOD_NAME,'side-switch',function(e,p){faSetting.set(F.sideCls,_this.o.body.hasClass('mini-side')?"":"mini-side");});FaEvent.onEvent(MOD_NAME,'refresh',function(event){util.info('refresh',event);});FaEvent.onEvent(MOD_NAME,'lock-screen',()=>{faSetting.set(F.lockScreenStatus,true);$(this.o.lockscreen).removeClass(this.o.hideCls);})
FaEvent.onEvent(MOD_NAME,'fullscreen',function(m,event){if(!document.fullscreenEnabled){util.info('unsupport fullscreen');return;}
if(document.fullscreenElement){_this.o.body.removeClass('fullscreen');document.exitFullscreen();}else{_this.o.body.addClass('fullscreen');document.body.requestFullscreen();}});$("#lock-password").on('input',(e)=>{let v=e.target.value;if(fa.options.lockPwd==v){e.target.value="";$(this.o.lockscreen).addClass(_this.o.hideCls);faSetting.set(F.lockScreenStatus,false);}})
if(faSetting.get(F.lockScreenStatus)){FaEvent.event(MOD_NAME,'lock-screen');}
util.info('init')}
exports(MOD_NAME,{head:new Fa_Head(),init:function(){this.head.init();}})})
fa.define(['layer','fa_util'],function(exports){'use strict';let MOD_NAME="fa_intro";let layer=layui.layer;let util=layui.fa_util;util.config({mod:MOD_NAME})
let FaIntro=function(){this.steps=[];this.idx=0;this.box;this.boxp;this.boxtool;this.options={name:'fa_intro',showTime:3,eleOncls:'fa-intro-on',}
this.init();}
FaIntro.prototype.noPlay=function(){let dict=layui.data(MOD_NAME);let c=dict[this.options.name]||0;util.log(c,this.options.showTime);if(c>this.options.showTime){return true;}
layui.data(MOD_NAME,{key:this.options.name,value:1+c})
return false;}
FaIntro.prototype.start=function(){if(this.noPlay()){return;}
this.shade();this.initBox();this.on(this.idx);}
FaIntro.prototype.close=function(){this.off();this.unShade();this.box.remove();this.boxp.remove();this.boxtool.remove();}
FaIntro.prototype.pre=function(){this.off(this.idx);this.on(--this.idx);}
FaIntro.prototype.next=function(){this.off(this.idx);this.on(++this.idx);}
FaIntro.prototype.initBox=function(){this.box=document.createElement('div');this.box.className="fa-intro-box";document.body.appendChild(this.box);this.boxp=document.createElement('div');this.boxp.className="fa-intro-boxp";document.body.appendChild(this.boxp);this.boxtool=document.createElement('div');this.boxtool.className="fa-intro-boxtool";this.boxtool.innerHTML=["<div id='intro_tip' class='tip'>提示</div>","<div class='btn'>","<a id='intro_pre' class='layui-btn layui-btn-xs'>上一步</a>","<a id='intro_next' class='layui-btn layui-btn-xs'>下一步</a>","</div>"].join("");document.body.appendChild(this.boxtool);this.intro_tip=document.getElementById('intro_tip');this.intro_pre=document.getElementById('intro_pre');this.intro_next=document.getElementById('intro_next');this.intro_pre.addEventListener('click',()=>{this.pre()})
this.intro_next.addEventListener('click',()=>{this.next()})}
FaIntro.prototype.resize=function(ele,rect){rect.w&&(ele.style.width=rect.w+"px");rect.h&&(ele.style.height=rect.h+"px");rect.l&&(ele.style.left=rect.l+"px");rect.t&&(ele.style.top=rect.t+"px");rect.r&&(ele.style.right=rect.r+"px");rect.b&&(ele.style.bottom=rect.b+"px");}
FaIntro.prototype.off=function(idx){if(!idx){this.steps.forEach(x=>util.removeClass(x.ele,this.options.eleOncls));return;}
if(idx>=0&&idx<this.steps.length){util.removeClass(this.steps[idx].ele,this.options.eleOncls);return;}}
FaIntro.prototype.on=function(idx){if(idx<0||idx>=this.steps.length){this.close();return;}
let step=this.steps[idx];let ele=step.ele;let intro=step.intro||"";if(!ele)return;let rect=ele.getBoundingClientRect(),opts={w:rect.width,h:rect.height,t:rect.y+window.scrollY,l:rect.x+window.scrollX};this.resize(this.box,opts);this.resize(this.boxp,opts);let bodyRect=document.body.getBoundingClientRect();let toolRect=this.boxtool.getBoundingClientRect();let x=opts.w+opts.l+10,y=opts.h+opts.t+10;if(bodyRect.width-x<opts.l){x=opts.l-toolRect.width-10}
if(bodyRect.height-y<opts.t){y=opts.t-toolRect.height-10}
if(x<0||toolRect.width+x>bodyRect.width){x=10;}
if(y<0||toolRect.height+y>bodyRect.height){y=10}
this.intro_tip.innerHTML=intro;this.resize(this.boxtool,{l:x,t:y});util.addClass(ele,this.options.eleOncls);}
FaIntro.prototype.fillSelf=function(ele){this.tipbox.innerHTML="";html2canvas(ele).then(canvas=>{this.tipbox.appendChild(canvas);});}
FaIntro.prototype.init=function(){this.steps=[];document.querySelectorAll('.fa-intro').forEach(x=>{this.steps.push({ele:x,intro:x.dataset['introDesc'],order:parseInt(x.dataset['introOrder'])});});this.steps.sort((a,b)=>{return a.order-b.order});util.log(this.steps);}
FaIntro.prototype.shade=function(){this.shadebox=document.createElement('div');this.shadebox.className='fa-intro-shade';this.shadebox.addEventListener('click',()=>{this.close();})
document.body.appendChild(this.shadebox);}
FaIntro.prototype.unShade=function(){this.shadebox.remove();}
FaIntro.prototype.setOptions=function(options){this.steps=options.steps||this.steps;delete options.steps;util.extend(this.options,options);}
exports(MOD_NAME,new FaIntro());});;fa.define(['jquery','laytpl','fa_util','fa_tab','fa_setting'],function(exports){let $=layui.$,fa_tab=layui.fa_tab,MOD_NAME="fa_nav",faSetting=layui.fa_setting,util=layui.fa_util;util.config({debug:true,module:MOD_NAME});let Fa_Nav=function(){this.options={nav_box:"fa-nav-box",nav_cls:"fa-nav",dir_cls:"fa-nav-dir",item_cls:"fa-nav-item",item_d_cls:"fa-nav-itemed",menuTpl:"menuTpl",shrink:true,menuUrl:"data/menu.json",title:(d)=>d.title,target:(d)=>d.target,children:(d)=>d.children,url:(d)=>d.url,icon:(d)=>d.icon,};}
Fa_Nav.prototype.init=function(options){util.extend(this.options,options||{});this.initUI();}
Fa_Nav.prototype.initEvent=function(){let _this=this,o=_this.options,box=$("."+o.nav_cls);faSetting.onChange(F.navMoreCls,true,(event,params)=>{util.toggleClass($("."+this.options.nav_box)[0],params.oldVal,params.val)});box.children("."+o.dir_cls).on('click',function(event){let self=$(this),isOn=self.hasClass(_this.options.item_d_cls);if(isOn){_this.closeDir(self);}else{_this.options.shrink&&_this.clearDir();_this.openDir(self)}
event.stopPropagation();return false;})
box.children("."+o.item_cls).on('click',function(event){_this.options.shrink&&_this.clearDir();_this.openItem($(this));event.stopPropagation();return false;})
util.info("init event")}
Fa_Nav.prototype.clearItem=function(){$("."+this.options.item_cls).removeClass(this.options.item_d_cls);}
Fa_Nav.prototype.clearDir=function(){$("."+this.options.dir_cls).removeClass(this.options.item_d_cls);}
Fa_Nav.prototype.myDir=function(ele){if(!ele.parent().hasClass('fa-nav-box')){return ele.parent().prev();}
return false;}
Fa_Nav.prototype.hasDir=function(ele){return ele.next().children("."+this.options.dir_cls)}
Fa_Nav.prototype.itemed=function(ele){ele.addClass(this.options.item_d_cls);let mydir=this.myDir(ele);mydir&&this.openDir(mydir);}
Fa_Nav.prototype.openDir=function(ele){let mydir=this.myDir(ele);ele.addClass(this.options.item_d_cls);mydir&&this.openDir(mydir);}
Fa_Nav.prototype.closeDir=function(ele){let hasdir=this.hasDir(ele)
util.info('hasdir',hasdir)
ele.removeClass(this.options.item_d_cls);hasdir&&hasdir.removeClass(this.options.item_d_cls);}
Fa_Nav.prototype.openUrl=function(opt){let url=opt.url,eleStor='.'+this.options.item_cls+'[fa-href="'+url+'"]';let ele=$(eleStor);if(ele.length>0){this.openItem(ele);}else{this.open(opt);}}
Fa_Nav.prototype.openItem=function(ele){this.clearItem();this.itemed(ele);let opt={title:ele.attr('title'),url:ele.attr('fa-href'),target:ele.attr('fa-target')}
this.open(opt);}
Fa_Nav.prototype.open=function(opt){if(opt.target.toUpperCase()==="_BLANK"){window.open(util.getPath(opt.url));return;}
if(opt.target.toUpperCase()==="JS"){eval(opt.url);return;}
fa_tab.open(opt.title,util.getPath(opt.url));}
Fa_Nav.prototype.initUI=function(){if(typeof this.options.reqData=='function'){this.options.reqData((data)=>{$("."+this.options.nav_box).html(this.build(data));this.initEvent();});}else{util.get(this.options.menuUrl,{},(resp)=>{$("."+this.options.nav_box).html(this.build(resp));this.initEvent();});}}
Fa_Nav.prototype.build=function(data){let result=[],children=this.options.children;data.forEach(((item)=>{if(children(item)&&children(item).length>0){result.push(this.buildDir(item));}else{result.push(this.buildItem(item));}}));return result.join('');}
Fa_Nav.prototype.buildDir=function(d){let title=this.options.title,icon=this.options.icon,children=this.options.children;return['<li class="fa-nav-dir">','   <em  class= "',icon(d),'" ></em>','   <span>',title(d),'</span>','</li >','<ul class="fa-nav">',this.build(children(d)),'</ul>'].join('');}
Fa_Nav.prototype.buildItem=function(d){let title=this.options.title,icon=this.options.icon,url=this.options.url,target=this.options.target;let r=[];r.push('<li class="fa-nav-item" title="',title(d),'" fa-href="',url(d),'" fa-target="',target(d),'">')
r.push('<em class="',icon(d),'"></em>');r.push('  <span>',title(d),'</span>');r.push('</li>');return r.join('');}
exports(MOD_NAME,{nav:new Fa_Nav(),init:function(options){this.nav.init(options);},open:function(opt={url:"",target:"_blank",title:"title"}){this.nav.openUrl(opt);}})});fa.define(['fa_util','jquery','fa_event','form'],function(exports){'use strict';let MOD_NAME="fa_setting",util=layui.fa_util,$=layui.jquery,form=layui.form,FaEvent=layui.fa_event;util.config({module:MOD_NAME});let Fa_Setting=function(){this.o={setCtl:$(".fa-ctl"),thmList:$(".fa-ctl-theme"),}};Fa_Setting.prototype.init=function(){this.initEvent();this.initUI();util.info('init');}
Fa_Setting.prototype.initUI=function(){this.o.thmList.children("[alt='"+this.get(F.themeCls)+"']").addClass('on');this.o.thmList.children("img").on('click',(e)=>{this.o.thmList.children(".on").removeClass('on');let thm=$(e.currentTarget).addClass('on').attr('alt');this.set(F.themeCls,thm);});form.on('radio(navMoreCls)',(data)=>{let navMoreCls=data.value;this.set(F.navMoreCls,navMoreCls);});form.val('setting',{'navMoreCls':this.get(F.navMoreCls)});form.on('checkbox(animStatus)',(data)=>{if(data.elem.checked){this.set(F.animStatus,data.value);}else{this.set(F.animStatus,"");}})
form.on('checkbox(holdSideStatus)',(data)=>{this.set(F.holdSideStatus,data.elem.checked);})
form.val('setting',{'animStatus':this.get(F.animStatus)&&true,'holdSideStatus':this.get(F.holdSideStatus)&&true,});}
Fa_Setting.prototype.initEvent=function(){FaEvent.onEvent("*",'setting',(event,params)=>{this.o.setCtl.toggleClass('layui-hide');});}
Fa_Setting.prototype.set=function(key,val){let oldVal=this.get(key);layui.data(MOD_NAME,{key:key,value:val});this.notice(key,val,oldVal);}
Fa_Setting.prototype.get=function(key){let setting=layui.data(MOD_NAME);if(key){return setting&&setting[key];}
return setting;}
Fa_Setting.prototype.onChange=function(key,init,func){FaEvent.onEvent(MOD_NAME,'change_'+key,func);let val=this.get(key);if(init&&val){func.call(this,{mod:MOD_NAME},{key:key,oldVal:"",val:val});}}
Fa_Setting.prototype.notice=function(key,val,oldVal){FaEvent.event(MOD_NAME,'change_'+key,{key:key,oldVal:oldVal,val:val});}
exports(MOD_NAME,{obj:new Fa_Setting(),init:function(){this.obj.init();},get:function(key){return this.obj.get(key);},set:function(key,val){this.obj.set(key,val);},onChange:function(key,init,func){this.obj.onChange(key,init,func);}});});;fa.define(['jquery','sha','fa_util','fa_event'],function(exports){"use strict";let $=layui.$,sha=layui.sha,MOD_NAME="fa_tab",util=layui.fa_util,faEvent=layui.fa_event;util.config({debug:true,module:MOD_NAME});let Fa_Tab=function(){this.onloads=[];this.options={tab:$('.fa-tab'),con:$('.fa-content'),pan:$('.fa-tab-pan ul'),itemd_cls:'fa-selected',con_item_cls:'.fa-content-item',item_cls:'.fa-tab-item',id_key:'fa-tab-id',prev:$('.fa-tab-prev'),next:$('.fa-tab-next'),ctr:$('.fa-tab-ctr'),ctr_more:$('.fa-tab-ctr-more'),lock_cls:'fa-tab-locked',tabWidth:0}}
Fa_Tab.prototype.init=function(options){let _this=this;$.extend(_this.options,options);this.resize();this.initToolBar();faEvent.onEvent("*",'refresh',()=>{this.ctr('refresh')});}
Fa_Tab.prototype.initToolBar=function(){let _this=this;this.options.prev.on('click',function(e){_this.scroll(_this.options.pan.innerWidth()/2);})
this.options.next.on('click',function(e){_this.scroll(_this.options.pan.innerWidth()/-2);})
this.options.ctr.on('click',function(e){_this.options.ctr_more.toggleClass('fa-tab-ctr-more-on')})
this.options.ctr_more.children('li').on('click',function(e){let tt=$(e.target).attr('fa-event');_this.ctr(tt);})}
Fa_Tab.prototype.scroll=function(len){util.debug(len,this.options.pan.innerWidth(),this.options.tabWidth);let pan=this.options.pan;len=len+(parseInt(pan.css('left')||0));len=len>0?0:len;let minLeft=this.options.pan.innerWidth()-this.options.tabWidth;minLeft=(minLeft>0)?0:minLeft;len=len<minLeft?minLeft:len;pan.css('left',len);}
Fa_Tab.prototype.showTab=function(idx){let pan=this.options.pan;let len=this.tabLeft[idx];let minLeft=this.options.pan.innerWidth()-len;let nowLeft=parseInt(pan.css('left')||0);minLeft=minLeft>0?0:minLeft;if(nowLeft<minLeft&&(nowLeft>(-len))){return;}
pan.css('left',minLeft);}
Fa_Tab.prototype.ctr=function(tt){let _this=this;switch(tt){case'refresh':let ss=$(_this.cons[_this.idx].children[0]);ss.attr('src',ss.attr('src'));break;case'closeOth':_this.tabs.each(function(idx,item){if(_this.idx!=idx&&!$(item).hasClass(_this.options.lock_cls)){$(item).remove();$(_this.cons[idx]).remove();}});_this.resize();break;case'closeAll':_this.tabs.each(function(idx,item){if(!$(item).hasClass(_this.options.lock_cls)){$(item).remove();$(_this.cons[idx]).remove();}else{_this.show(item);}});_this.resize();break;}}
Fa_Tab.prototype.show=function(ele){let idx=parseInt($(ele).attr(this.options.id_key));if(this.idx==idx){return;}
$(this.tabs[this.idx]).removeClass(this.options.itemd_cls);$(this.cons[this.idx]).hide();this.idx=idx;$(this.tabs[this.idx]).addClass(this.options.itemd_cls);$(this.cons[this.idx]).show();this.showTab(idx);}
Fa_Tab.prototype.buildCon=function(url){let con=document.createElement('div');con.className='fa-content-item';let iframe=document.createElement('iframe');iframe.src=url;iframe.addEventListener('load',(e)=>{util.info(iframe.contentWindow,'iframe loaded');this.onloads.forEach(func=>{func(iframe.contentWindow);});});con.append(iframe);this.options.con.append(con);}
Fa_Tab.prototype.buildTab=function(uid,title,lock=false){let r=['<li class="fa-tab-item  ',(lock?this.options.lock_cls:""),'" uid="',uid,'"',this.options.id_key,'=',this.cons.size(),'><span>',title,'</span><i class="layui-icon layui-icon-close"></i></li>'].join('')
this.options.pan.append(r);}
Fa_Tab.prototype.close=function(ele){if($(ele).hasClass(this.options.lock_cls)){return;}
let idx=parseInt($(ele).attr(this.options.id_key));if(idx==this.idx){let n=this.tabs[(idx+1)>=this.tabs.size()?(idx-1):(idx+1)];this.show(n);}
$(this.tabs[idx]).remove();$(this.cons[idx]).remove();this.resize();}
Fa_Tab.prototype.open=function(title,url,lock=false){let _this=this,hasOpened=false,uid=sha.hex_sha1(url);this.tabs.each(function(idx,item){if(uid==$(item).attr('uid')){_this.show(item);hasOpened=true;return;}});if(hasOpened){return;}
this.buildTab(uid,title,lock)
this.buildCon(url);this.resize();this.show(this.tabs[this.tabs.size()-1]);}
Fa_Tab.prototype.resize=function(){let _this=this,o=this.options,tab=o.tab,con=o.con;this.tabs=tab.find(o.item_cls);this.cons=con.find(o.con_item_cls);this.tabLeft=[];_this.options.tabWidth=0;this.tabs.each(function(idx,item){$(item).attr(o.id_key,idx);_this.options.tabWidth+=$(item).outerWidth();_this.tabLeft[idx]=_this.options.tabWidth;});this.show(this.options.tab.find("."+this.options.itemd_cls)[0]);this.tabs.unbind('click');this.tabs.find('.layui-icon-close').unbind('click');this.tabs.on('click',function(event){_this.show(this);event.stopPropagation();return false;})
this.tabs.find('.layui-icon-close').on('click',function(event){_this.close($(this).parent(_this.options.item_cls));event.stopPropagation();return false;})}
exports(MOD_NAME,{tab:new Fa_Tab(),init:function(){this.tab.init({a:1});},open:function(title,url,lock=false){this.tab.open(title,url,lock);},refresh:function(){this.tab.ctr('refresh');},onload:function(cb){this.tab.onloads.push(cb);}});});;fa.define(['fa_util','jquery','fa_event'],function(exports){'use strict';let MOD_NAME="fa_todolist",util=layui.fa_util,$=layui.jquery,FaEvent=layui.fa_event;util.config({debug:true,module:MOD_NAME});let Fa_ToDoList=function(){this.init();};Fa_ToDoList.prototype.init=function(){}
exports(MOD_NAME,{o:new Fa_ToDoList(),})});;fa.define(['jquery'],function(exports){'use strict';let $=layui.jquery;let FaUtil=function(){this.opts={debug:fa.options.debug,module:"util"};};FaUtil.prototype.log=function(...msg){this.opts.debug&&console.log("FA",this.opts.module,...msg)}
FaUtil.prototype.info=function info(...msg){this.opts.debug&&console.info("FA",this.opts.module,...msg)}
FaUtil.prototype.debug=function debug(...msg){this.opts.debug&&this.debug&&console.debug("FA",this.opts.module,...msg)}
FaUtil.prototype.error=function debug(...msg){this.opts.debug&&console.error("FA",this.opts.module,...msg)}
FaUtil.prototype.toggleClass=function(ele,lastClz,newClaz){this.removeClass(ele,lastClz);this.addClass(ele,newClaz);}
FaUtil.prototype.removeClass=function(ele,clazz){let clz=ele.getAttribute('class')||"";ele.setAttribute('class',clz.split(' ').filter(x=>x!==clazz&&x!=="").join(' '));}
FaUtil.prototype.addClass=function(ele,clazz){let clz=ele.getAttribute('class');ele.setAttribute('class',clz+' '+clazz);}
FaUtil.prototype.config=function(opts={debug:true,module:"module"}){this.opts.module=opts.module||this.opts.module;}
FaUtil.prototype.get=function(url,data,ok,fail){$.ajax(this.getPath(url),{method:"GET",dataType:"JSON",data:data,success:ok,error:fail})}
FaUtil.prototype.getPath=function(path){if(path.startsWith("/")||path.match("[a-zA-z]+://[^\s]*")){return path;}
return workpath+path;}
FaUtil.prototype.colorWord=function(word){let o=[];for(var i=0;i<word.length;i++){o.push(word.charAt(i));}
let colors=["#009688","#5FB878","#FFB800","#FF5722","#01AAED","#1E9FFF","#2F4056"];colors.sort((a,b)=>(.5-Math.random()));let r=o.map((x,y)=>{return['<span style="color:',colors[y%colors.length],'">',x,'</span>'].join('')})
return r.join('');}
FaUtil.prototype.extend=function(){var target=arguments[0]||{},i=1,length=arguments.length,options;if(typeof target!="object"&&typeof target!="function")
target={};for(;i<length;i++){if((options=arguments[i])!=null){for(var name in options){var copy=options[name];if(target===copy){continue;}
if(copy!==undefined){target[name]=copy;}}}}
return target;}
exports('fa_util',new FaUtil());});;fa.define(function(exports){var hexcase=0;var b64pad="";var chrsz=8;let api={hex_sha1:function(s){return binb2hex(core_sha1(str2binb(s),s.length*chrsz));},b64_sha1:function(s){return binb2b64(core_sha1(str2binb(s),s.length*chrsz));},str_sha1:function(s){return binb2str(core_sha1(str2binb(s),s.length*chrsz));},hex_hmac_sha1:function(key,data){return binb2hex(core_hmac_sha1(key,data));},b64_hmac_sha1:function(key,data){return binb2b64(core_hmac_sha1(key,data));},str_hmac_sha1:function(key,data){return binb2str(core_hmac_sha1(key,data));}}
exports('sha',api);function core_sha1(x,len){x[len>>5]|=0x80<<(24-len%32);x[((len+64>>9)<<4)+15]=len;var w=Array(80);var a=1732584193;var b=-271733879;var c=-1732584194;var d=271733878;var e=-1009589776;for(var i=0;i<x.length;i+=16){var olda=a;var oldb=b;var oldc=c;var oldd=d;var olde=e;for(var j=0;j<80;j++){if(j<16)w[j]=x[i+j];else w[j]=rol(w[j-3]^w[j-8]^w[j-14]^w[j-16],1);var t=safe_add(safe_add(rol(a,5),sha1_ft(j,b,c,d)),safe_add(safe_add(e,w[j]),sha1_kt(j)));e=d;d=c;c=rol(b,30);b=a;a=t;}
a=safe_add(a,olda);b=safe_add(b,oldb);c=safe_add(c,oldc);d=safe_add(d,oldd);e=safe_add(e,olde);}
return Array(a,b,c,d,e);}
function sha1_ft(t,b,c,d){if(t<20)return(b&c)|((~b)&d);if(t<40)return b^c^d;if(t<60)return(b&c)|(b&d)|(c&d);return b^c^d;}
function sha1_kt(t){return(t<20)?1518500249:(t<40)?1859775393:(t<60)?-1894007588:-899497514;}
function core_hmac_sha1(key,data){var bkey=str2binb(key);if(bkey.length>16)bkey=core_sha1(bkey,key.length*chrsz);var ipad=Array(16),opad=Array(16);for(var i=0;i<16;i++){ipad[i]=bkey[i]^0x36363636;opad[i]=bkey[i]^0x5C5C5C5C;}
var hash=core_sha1(ipad.concat(str2binb(data)),512+data.length*chrsz);return core_sha1(opad.concat(hash),512+160);}
function safe_add(x,y){var lsw=(x&0xFFFF)+(y&0xFFFF);var msw=(x>>16)+(y>>16)+(lsw>>16);return(msw<<16)|(lsw&0xFFFF);}
function rol(num,cnt){return(num<<cnt)|(num>>>(32-cnt));}
function str2binb(str){var bin=Array();var mask=(1<<chrsz)-1;for(var i=0;i<str.length*chrsz;i+=chrsz)
bin[i>>5]|=(str.charCodeAt(i/chrsz)&mask)<<(24-i%32);return bin;}
function binb2str(bin){var str="";var mask=(1<<chrsz)-1;for(var i=0;i<bin.length*32;i+=chrsz)
str+=String.fromCharCode((bin[i>>5]>>>(24-i%32))&mask);return str;}
function binb2hex(binarray){var hex_tab=hexcase?"0123456789ABCDEF":"0123456789abcdef";var str="";for(var i=0;i<binarray.length*4;i++){str+=hex_tab.charAt((binarray[i>>2]>>((3-i%4)*8+4))&0xF)+hex_tab.charAt((binarray[i>>2]>>((3-i%4)*8))&0xF);}
return str;}
function binb2b64(binarray){var tab="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";var str="";for(var i=0;i<binarray.length*4;i+=3){var triplet=(((binarray[i>>2]>>8*(3-i%4))&0xFF)<<16)|(((binarray[i+1>>2]>>8*(3-(i+1)%4))&0xFF)<<8)|((binarray[i+2>>2]>>8*(3-(i+2)%4))&0xFF);for(var j=0;j<4;j++){if(i*8+j*6>binarray.length*32)str+=b64pad;else str+=tab.charAt((triplet>>6*(3-j))&0x3F);}}
return str;}})