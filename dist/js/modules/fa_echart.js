/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['jquery'],function(exports){'use static';let $=layui.jquery;let FaEchart=function(){};FaEchart.prototype.echart=function(view,options){var myChart=echarts.init(document.querySelector(view));myChart.setOption(options);return myChart;}
const OPTS={line:function(opt={xTitle:[],series:[]}){let def={legend:{},grid:{left:'3%',right:'4%',bottom:'3%',containLabel:true},tooltip:{trigger:'axis'},xAxis:{type:'category',boundaryGap:true,data:opt.xTitle||[]},yAxis:{type:'value'},series:opt.series||[]};return def;},heatmap:function(opt={xTitle:[],yTitle:[],min:0,max:0,name:"",data:[],label:false}){let def={tooltip:{},grid:{left:'1%',right:'1%',top:'2%',bottom:'15%',containLabel:true},xAxis:{type:'category',data:opt.xTitle||[],splitArea:{show:true}},yAxis:{type:'category',data:opt.yTitle||[],splitArea:{show:true}},visualMap:{min:opt.min||0,max:opt.max||10,calculable:true,orient:'horizontal',left:'center',bottom:'0%',itemWidth:14},series:[{name:opt.name||"name",type:'heatmap',data:opt.data||[],label:{show:opt.label||true},emphasis:{itemStyle:{shadowBlur:10,shadowOffsetX:0,shadowColor:'rgba(0, 0, 0, 0.5)'}}}]}
return def;},pie:function(opt={name:"",radius:'',data:[]}){let def={tooltip:{trigger:'item'},series:[{name:opt.name||'',type:'pie',radius:opt.radius||'80%',data:opt.data||[],emphasis:{itemStyle:{shadowBlur:10,shadowOffsetX:0,shadowColor:'rgba(0, 0, 0, 0.5)'}}}]};return def;},bar:function(opt={xTitle:[],data:[]}){let def={legend:{},tooltip:{},grid:{left:'3%',right:'4%',bottom:'3%',containLabel:true},xAxis:{type:'category',data:opt.xTitle||[]},yAxis:{type:'value'},series:opt.data||[]};return def;},radar:function(opt={legend:[],name:"",data:[],indicator:[]}){let def={tooltip:{},legend:{data:opt.legend||[]},radar:{indicator:opt.indicator||[]},series:[{name:opt.name||'',type:'radar',data:opt.data||[]}]}
return def;}}
FaEchart.prototype.buildOpts=function(type,options={legend:{},xTitle:[],series:[]}){let opts=OPTS[type](options);if(opts.config){$.extend(opts,options.config);}
return opts;}
exports('fa_echart',new FaEchart());})