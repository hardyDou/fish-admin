/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['jquery'],function(exports){'use strict';let $=layui.jquery;let FaUtil=function(opts={module:"util"}){this.opts=this.extend(this.opts,opts);};FaUtil.prototype.toggleClass=function(ele,lastClz,newClaz){this.removeClass(ele,lastClz);this.addClass(ele,newClaz);}
FaUtil.prototype.removeClass=function(ele,clazz){let clz=ele.getAttribute('class')||"";ele.setAttribute('class',clz.split(' ').filter(x=>x!==clazz&&x!=="").join(' '));}
FaUtil.prototype.addClass=function(ele,clazz){let clz=ele.getAttribute('class');ele.setAttribute('class',clz+' '+clazz);}
FaUtil.prototype.get=function(url,data,ok,fail){$.ajax(this.getPath(url),{method:"GET",dataType:"JSON",data:data,success:ok,error:fail})}
FaUtil.prototype.getPath=function(path){if(path.startsWith("/")||path.match("[a-zA-z]+://[^\s]*")){return path;}
return workpath+path;}
FaUtil.prototype.colorWord=function(word){let o=[];for(var i=0;i<word.length;i++){o.push(word.charAt(i));}
let colors=["#009688","#5FB878","#FFB800","#FF5722","#01AAED","#1E9FFF","#2F4056"];colors.sort((a,b)=>(.5-Math.random()));let r=o.map((x,y)=>{return['<span style="color:',colors[y%colors.length],'">',x,'</span>'].join('')})
return r.join('');}
FaUtil.prototype.extend=function(){var target=arguments[0]||{},i=1,length=arguments.length,options;if(typeof target!="object"&&typeof target!="function")
target={};for(;i<length;i++){if((options=arguments[i])!=null){for(var name in options){var copy=options[name];if(target===copy){continue;}
if(copy!==undefined){target[name]=copy;}}}}
return target;}
FaUtil.prototype.rect=function(ele){if(ele instanceof $){ele=ele[0];}
let rect=ele.getBoundingClientRect();return{width:rect.width,height:rect.height,top:rect.y+window.scrollY,left:rect.x+window.scrollX};}
exports('fa_util',new FaUtil());});