/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['jquery','fa_log','fa_util','fa_event','fa_setting'],function(exports){let $=layui.$,MOD_NAME="fa_head",FaEvent=layui.fa_event,faSetting=layui.fa_setting,util=layui.fa_util,log=layui.fa_log.getLog(MOD_NAME);let Fa_Head=function(){this.o={box:'.fa-header',lockscreen:".fa-lock-box",sideSwitch:$('.fa-side-switch'),refreshBtn:$('.fa-refresh'),body:$('body'),side_off:'mini-side',hideCls:'fa-hide'}}
Fa_Head.prototype.init=function(){let _this=this;$("*[fa-event]").on('click',function(e){let event=$(e.currentTarget).attr('fa-event');event&&FaEvent.event(MOD_NAME,event,e);});FaEvent.onEvent(MOD_NAME,'side-switch',function(e,p){faSetting.set(F.sideCls,_this.o.body.hasClass('mini-side')?"":"mini-side");});FaEvent.onEvent(MOD_NAME,'refresh',function(event){log.info('refresh',event);});FaEvent.onEvent(MOD_NAME,'lock-screen',()=>{faSetting.set(F.lockScreenStatus,true);$(this.o.lockscreen).removeClass(this.o.hideCls);})
FaEvent.onEvent(MOD_NAME,'fullscreen',function(m,event){if(!document.fullscreenEnabled){log.info('unsupport fullscreen');return;}
if(document.fullscreenElement){_this.o.body.removeClass('fullscreen');document.exitFullscreen();}else{_this.o.body.addClass('fullscreen');document.body.requestFullscreen();}});$("#lock-password").on('input',(e)=>{let v=e.target.value;if(fa.options.lockPwd==v){e.target.value="";$(this.o.lockscreen).addClass(_this.o.hideCls);faSetting.set(F.lockScreenStatus,false);}})
if(faSetting.get(F.lockScreenStatus)){FaEvent.event(MOD_NAME,'lock-screen');}
log.info('init')}
exports(MOD_NAME,{head:new Fa_Head(),init:function(){this.head.init();}})})