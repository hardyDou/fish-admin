/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

fa.define(["jquery","fa_util","fa_log"],function(exports){let name="fa_dropdown";let $=layui.jquery;let log=layui.fa_log.getLog(name);let util=layui.fa_util;let cls=".fa-dropdown";let FaDropdown=function(){};let eventHandler=function(event){log.info('event : ',event);}
function buildItem(x){return['<li data-dp-event="',x.event,'" class="fa-dropdown-item ',x.icon,'">',x.title,'</li>'].join('')}
let OffsetLogic={'l':function(r1,r2){return{top:r1.top+r1.height/2-r2.height/2,left:r1.left-r2.width}},'r':function(r1,r2){return{left:r1.left+r1.width,top:r1.top+r1.height/2-r2.height/2}},'t':function(r1,r2){return{top:r1.top-r2.height,left:r1.left+r1.width/2-r2.width/2}},'b':function(r1,r2){return{top:r1.top+r1.height,left:r1.left+r1.width/2-r2.width/2}}}
let offsetCls={l:"left",r:"right",b:"bottom",t:"top"}
function fixed(o,con,offset){con.addClass(offsetCls[offset]);let r1=util.rect(o),r2=util.rect(con);let r3=OffsetLogic[offset](r1,r2);con.css({left:r3.left,top:r3.top});}
FaDropdown.prototype.render=function(opt){let eh=eventHandler;if(typeof opt.eventHandler=="function"){eh=opt.eventHandler;}
opt=opt||{};$(cls).on('click',function(e){$(".fa-dropdown-con").remove();let o=$(e.target);let id=o.data("dp-id");let content=opt[id]||{};if(!content.menus||content.menus.length==0){log.info(id,'has no con !');return;}
let con=['<ul  class="fa-dropdown-con" style="top:-30000px;">'];content.menus.forEach((x)=>{con.push(buildItem(x));});con.push('</ul>');let n=$(con.join(''));n.on('click',(e)=>{eh($(e.target).data('dp-event'))
e.stopPropagation();});$('body').append(n);fixed(o,n,content.offset);$('body').on('click',(e)=>{n.remove();e.preventDefault();})
e.stopPropagation();});}
exports(name,new FaDropdown())});