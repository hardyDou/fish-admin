/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['fa_log','fa_util'],function(exports){'use strict';let MOD_NAME='fa_event',util=layui.fa_util,log=layui.fa_log.getLog(MOD_NAME);let Fa_Event=function(){this.init();}
Fa_Event.prototype.init=function(){this.EL={};}
Fa_Event.prototype.event=function(mod,event,params){let mods=["*",mod];let faEvent={mod:mod,event:event};mods.forEach(m=>{let key=this.genKey(m,event);this.EL[key]=this.EL[key]||[];this.EL[key].forEach(cb=>{cb&&(typeof cb==='function')&&cb.call(this,faEvent,params);});})}
Fa_Event.prototype.onEvent=function(mod,event,callback){let key=this.genKey(mod,event);this.EL[key]=this.EL[key]||[];this.EL[key].push(callback);}
Fa_Event.prototype.genKey=function(mod,event){return mod+"_@FA@_"+event;}
exports(MOD_NAME,{e:new Fa_Event(),event:function(mod,event,params){this.e.event(mod,event,params);},onEvent:function(mod,event,callback){this.e.onEvent(mod,event,callback);}});});