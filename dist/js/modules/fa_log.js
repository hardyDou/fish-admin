/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

fa.define(function(exports){'use strict';let FaLog=function(mod){this.level=fa.options.LOG_LEVEL||this.INFO;this.mod=mod||"log";};FaLog.prototype.DEBUG=3;FaLog.prototype.INFO=2;FaLog.prototype.ERROR=1;FaLog.prototype.debug=function(...msg){(this.level>=this.DEBUG)&&console.debug("Fa [DEBUG]",this.mod,...msg);}
FaLog.prototype.log=function(...msg){(this.level>=this.INFO)&&console.log("Fa [INFO]",this.mod,...msg);}
FaLog.prototype.info=function(...msg){(this.level>=this.INFO)&&console.info("Fa [INFO]",this.mod,...msg);}
FaLog.prototype.error=function(...msg){(this.level>=this.ERROR)&&console.error("Fa [ERROR]",this.mod,...msg);}
exports('fa_log',{getLog:function(mod){return new FaLog(mod);}});});