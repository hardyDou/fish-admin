/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

fa.define(['layer','fa_util','fa_log'],function(exports){'use strict';let MOD_NAME="fa_intro";let layer=layui.layer;let util=layui.fa_util,log=layui.fa_log.getLog(MOD_NAME);let FaIntro=function(){this.steps=[];this.idx=0;this.box;this.boxp;this.boxtool;this.options={name:'fa_intro',showTime:3,eleOncls:'fa-intro-on',}
this.init();}
FaIntro.prototype.noPlay=function(){let dict=layui.data(MOD_NAME);let c=dict[this.options.name]||0;log.log(c,this.options.showTime);if(c>this.options.showTime){return true;}
layui.data(MOD_NAME,{key:this.options.name,value:1+c})
return false;}
FaIntro.prototype.start=function(){if(this.noPlay()){return;}
this.shade();this.initBox();this.on(this.idx);}
FaIntro.prototype.close=function(){this.off();this.unShade();this.box.remove();this.boxp.remove();this.boxtool.remove();}
FaIntro.prototype.pre=function(){this.off(this.idx);this.on(--this.idx);}
FaIntro.prototype.next=function(){this.off(this.idx);this.on(++this.idx);}
FaIntro.prototype.initBox=function(){this.box=document.createElement('div');this.box.className="fa-intro-box";document.body.appendChild(this.box);this.boxp=document.createElement('div');this.boxp.className="fa-intro-boxp";document.body.appendChild(this.boxp);this.boxtool=document.createElement('div');this.boxtool.className="fa-intro-boxtool";this.boxtool.innerHTML=["<div id='intro_tip' class='tip'>提示</div>","<div class='btn'>","<a id='intro_pre' class='layui-btn layui-btn-xs'>上一步</a>","<a id='intro_next' class='layui-btn layui-btn-xs'>下一步</a>","</div>"].join("");document.body.appendChild(this.boxtool);this.intro_tip=document.getElementById('intro_tip');this.intro_pre=document.getElementById('intro_pre');this.intro_next=document.getElementById('intro_next');this.intro_pre.addEventListener('click',()=>{this.pre()})
this.intro_next.addEventListener('click',()=>{this.next()})}
FaIntro.prototype.resize=function(ele,rect){rect.w&&(ele.style.width=rect.w+"px");rect.h&&(ele.style.height=rect.h+"px");rect.l&&(ele.style.left=rect.l+"px");rect.t&&(ele.style.top=rect.t+"px");rect.r&&(ele.style.right=rect.r+"px");rect.b&&(ele.style.bottom=rect.b+"px");}
FaIntro.prototype.off=function(idx){if(!idx){this.steps.forEach(x=>util.removeClass(x.ele,this.options.eleOncls));return;}
if(idx>=0&&idx<this.steps.length){util.removeClass(this.steps[idx].ele,this.options.eleOncls);return;}}
FaIntro.prototype.on=function(idx){if(idx<0||idx>=this.steps.length){this.close();return;}
let step=this.steps[idx];let ele=step.ele;let intro=step.intro||"";if(!ele)return;let rect=ele.getBoundingClientRect(),opts={w:rect.width,h:rect.height,t:rect.y+window.scrollY,l:rect.x+window.scrollX};this.resize(this.box,opts);this.resize(this.boxp,opts);let bodyRect=document.body.getBoundingClientRect();let toolRect=this.boxtool.getBoundingClientRect();let x=opts.w+opts.l+10,y=opts.h+opts.t+10;if(bodyRect.width-x<opts.l){x=opts.l-toolRect.width-10}
if(bodyRect.height-y<opts.t){y=opts.t-toolRect.height-10}
if(x<0||toolRect.width+x>bodyRect.width){x=10;}
if(y<0||toolRect.height+y>bodyRect.height){y=10}
this.intro_tip.innerHTML=intro;this.resize(this.boxtool,{l:x,t:y});util.addClass(ele,this.options.eleOncls);}
FaIntro.prototype.fillSelf=function(ele){this.tipbox.innerHTML="";html2canvas(ele).then(canvas=>{this.tipbox.appendChild(canvas);});}
FaIntro.prototype.init=function(){this.steps=[];document.querySelectorAll('.fa-intro').forEach(x=>{this.steps.push({ele:x,intro:x.dataset['introDesc'],order:parseInt(x.dataset['introOrder'])});});this.steps.sort((a,b)=>{return a.order-b.order});log.log(this.steps);}
FaIntro.prototype.shade=function(){this.shadebox=document.createElement('div');this.shadebox.className='fa-intro-shade';this.shadebox.addEventListener('click',()=>{this.close();})
document.body.appendChild(this.shadebox);}
FaIntro.prototype.unShade=function(){this.shadebox.remove();}
FaIntro.prototype.setOptions=function(options){this.steps=options.steps||this.steps;delete options.steps;util.extend(this.options,options);}
exports(MOD_NAME,new FaIntro());});