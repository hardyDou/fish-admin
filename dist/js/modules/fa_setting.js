/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['fa_log','fa_drawer','jquery','fa_event','form'],function(exports){'use strict';let MOD_NAME="fa_setting",$=layui.jquery,form=layui.form,log=layui.fa_log.getLog(MOD_NAME),drawer=layui.fa_drawer,FaEvent=layui.fa_event;let Fa_Setting=function(){};Fa_Setting.prototype.init=function(){this.initEvent();}
Fa_Setting.prototype.show=function(){drawer.render({offset:'r',width:"300px",top:50,bottom:40,content:$("#setting").html()});let thmList=$(".fa-ctl-theme");thmList.children("[alt='"+this.get(F.themeCls)+"']").addClass('on');thmList.children("img").on('click',(e)=>{thmList.children(".on").removeClass('on');let thm=$(e.currentTarget).addClass('on').attr('alt');this.set(F.themeCls,thm);});form.on('radio(navMoreCls)',(data)=>{let navMoreCls=data.value;this.set(F.navMoreCls,navMoreCls);});form.val('setting',{'navMoreCls':this.get(F.navMoreCls)});form.on('checkbox(animStatus)',(data)=>{if(data.elem.checked){this.set(F.animStatus,data.value);}else{this.set(F.animStatus,"");}})
form.on('checkbox(holdSideStatus)',(data)=>{this.set(F.holdSideStatus,data.elem.checked);})
form.val('setting',{'animStatus':this.get(F.animStatus)&&true,'holdSideStatus':this.get(F.holdSideStatus)&&true,});}
Fa_Setting.prototype.initEvent=function(){FaEvent.onEvent("*",'setting',(event,params)=>{this.show();});}
Fa_Setting.prototype.set=function(key,val){let oldVal=this.get(key);layui.data(MOD_NAME,{key:key,value:val});this.notice(key,val,oldVal);}
Fa_Setting.prototype.get=function(key){let setting=layui.data(MOD_NAME);if(key){return setting&&setting[key];}
return setting;}
Fa_Setting.prototype.onChange=function(key,init,func){FaEvent.onEvent(MOD_NAME,'change_'+key,func);let val=this.get(key);if(init&&val){func.call(this,{mod:MOD_NAME},{key:key,oldVal:"",val:val});}}
Fa_Setting.prototype.notice=function(key,val,oldVal){FaEvent.event(MOD_NAME,'change_'+key,{key:key,oldVal:oldVal,val:val});}
exports(MOD_NAME,{obj:new Fa_Setting(),init:function(){this.obj.init();},get:function(key){return this.obj.get(key);},set:function(key,val){this.obj.set(key,val);},onChange:function(key,init,func){this.obj.onChange(key,init,func);}});});