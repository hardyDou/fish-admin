/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

;fa.define(['jquery','fa_setting','fa_util','fa_event'],function(exports){"use strict";let $=layui.$,faSetting=layui.fa_setting,MOD_NAME="fa_tab",log=layui.fa_log.getLog(MOD_NAME),faEvent=layui.fa_event;let Fa_Tab=function(){this.idx=-1;this.onloads=[];this.options={tab:$('.fa-tab'),con:$('.fa-content'),pan_box:$('.fa-tab-pan'),pan:$('.fa-tab-pan ul'),itemd_cls:'fa-selected',con_item_cls:'.fa-content-item',item_cls:'.fa-tab-item',id_key:'fa-tab-id',prev:$('.fa-tab-prev'),next:$('.fa-tab-next'),ctr:$('.fa-tab-ctr'),ctr_more:$('.fa-tab-ctr-more'),lock_cls:'fa-tab-locked',tabWidth:0}}
Fa_Tab.prototype.init=function(options){let _this=this;$.extend(_this.options,options);this.resize();this.initToolBar();faEvent.onEvent("*",'refresh',()=>{this.ctr('refresh')});}
Fa_Tab.prototype.initToolBar=function(){let _this=this;this.options.prev.on('click',function(e){_this.scroll(_this.options.pan_box.innerWidth()/2);})
this.options.next.on('click',function(e){_this.scroll(_this.options.pan_box.innerWidth()/-2);})
this.options.ctr.on('click',function(e){_this.options.ctr_more.toggleClass('fa-tab-ctr-more-on')})
this.options.ctr_more.children('li').on('click',function(e){let tt=$(e.target).attr('fa-event');_this.ctr(tt);})}
Fa_Tab.prototype.scroll=function(len){let pan=this.options.pan;len=len+(parseInt(pan.css('left')||0));len=len>0?0:len;let minLeft=this.options.pan_box.innerWidth()-this.options.tabWidth;minLeft=(minLeft>0)?0:minLeft;len=len<minLeft?minLeft:len;pan.css('left',len);}
Fa_Tab.prototype.showTab=function(idx){let pan=this.options.pan;let len=this.tabLeft[idx];let minLeft=this.options.pan_box.innerWidth()-len;let nowLeft=parseInt(pan.css('left')||0);minLeft=minLeft>0?0:minLeft;if(nowLeft<minLeft&&(nowLeft>(-len))){return;}
pan.css('left',minLeft);}
Fa_Tab.prototype.ctr=function(tt){let _this=this;switch(tt){case'refresh':let ss=$(_this.cons[_this.idx].children[0]);ss.attr('src',ss.attr('src'));break;case'closeOth':_this.tabs.each(function(idx,item){log.info($(item).hasClass(_this.options.lock_cls));if(_this.idx!=idx&&!$(item).hasClass(_this.options.lock_cls)){$(item).remove();$(_this.cons[idx]).remove();}});_this.resize();break;case'closeAll':_this.tabs.each(function(idx,item){if(!$(item).hasClass(_this.options.lock_cls)){$(item).remove();$(_this.cons[idx]).remove();}else{_this.show(item);}});_this.resize();break;}}
Fa_Tab.prototype.show=function(ele){let idx=parseInt($(ele).attr(this.options.id_key));if(this.idx==idx){return;}
$(this.tabs[this.idx]).removeClass(this.options.itemd_cls);$(this.cons[this.idx]).hide();this.idx=idx;$(this.tabs[this.idx]).addClass(this.options.itemd_cls);$(this.cons[this.idx]).show();let uid=$(ele).attr('uid'),title=$(ele).text();let lock=$(ele).hasClass(this.options.lock_cls);faSetting.set(F.lastPage,JSON.stringify({url:atob(uid),title:title,target:"TAB",lock:lock}));this.showTab(idx);}
Fa_Tab.prototype.buildCon=function(url){let con=document.createElement('div');con.className='fa-content-item';let iframe=document.createElement('iframe');iframe.src=url;iframe.addEventListener('load',(e)=>{log.info('iframe loaded');this.onloads.forEach(func=>{func(iframe.contentWindow);});});con.append(iframe);this.options.con.append(con);}
Fa_Tab.prototype.buildTab=function(uid,title,lock){let r=['<li class="fa-tab-item  ',(lock==true?this.options.lock_cls:""),'" uid="',uid,'"',this.options.id_key,'=',this.cons.size(),'><span>',title,'</span><i class="layui-icon layui-icon-close"></i></li>'].join('')
this.options.pan.append(r);}
Fa_Tab.prototype.close=function(ele){if($(ele).hasClass(this.options.lock_cls)){return;}
let idx=parseInt($(ele).attr(this.options.id_key));$(this.tabs[idx]).remove();$(this.cons[idx]).remove();if(idx==this.idx){let n=this.tabs[(idx+1)>=this.tabs.size()?(idx-1):(idx+1)];this.show(n);}else{this.show(tab.find("."+this.options.itemd_cls)[0]);}
this.resize();}
Fa_Tab.prototype.open=function(title,url,lock){let _this=this,hasOpened=false,uid=btoa(url);this.tabs.each(function(idx,item){if(uid==$(item).attr('uid')){_this.show(item);hasOpened=true;return;}});if(hasOpened){return;}
this.buildTab(uid,title,lock)
this.buildCon(url);this.resize();this.show(this.tabs[this.tabs.size()-1]);}
Fa_Tab.prototype.resize=function(){let _this=this,o=this.options,tab=o.tab,con=o.con;this.tabs=tab.find(o.item_cls);this.cons=con.find(o.con_item_cls);this.tabLeft=[];if(this.tabs.length==0){return;}
_this.options.tabWidth=0;this.tabs.each(function(idx,item){$(item).attr(o.id_key,idx);_this.options.tabWidth+=$(item).outerWidth();_this.tabLeft[idx]=_this.options.tabWidth;});this.tabs.unbind('click');this.tabs.find('.layui-icon-close').unbind('click');this.tabs.on('click',function(event){_this.show(this);event.stopPropagation();return false;})
this.tabs.find('.layui-icon-close').on('click',function(event){_this.close($(this).parent(_this.options.item_cls));event.stopPropagation();return false;})}
exports(MOD_NAME,{tab:new Fa_Tab(),init:function(){this.tab.init({a:1});},open:function(title,url,lock){this.tab.open(title,url,lock);},refresh:function(){this.tab.ctr('refresh');},onload:function(cb){this.tab.onloads.push(cb);}});});