/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

fa.define(['fa_log','fa_util','fa_setting'],function(exports){'use strict';let MOD_NAME="body_cls";let util=layui.fa_util,log=layui.fa_log.getLog(MOD_NAME);const listener=[window];let params={};function addListener(w){listener.push(w);util.toggleClass(w.document.body,params.oldVal,params.val);}
function change(np){params=np;listener.forEach(w=>{util.toggleClass(w.document.body,params.oldVal,params.val);})}
exports(MOD_NAME,{addListener:addListener,change:change});});