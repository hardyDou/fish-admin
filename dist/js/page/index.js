let loading;
fa.use(['layer'], function () {
    loading = layui.layer.load(3);
})
fa.use(['element', 'form', 'fa_setting', 'body_cls', 'fa_head', 'fa_nav', 'fa_tab'], function () {
    "use strict";
    let faSetting = layui.fa_setting,
        head = layui.fa_head,
        nav = layui.fa_nav,
        tab = layui.fa_tab,
        bodyCls = layui.body_cls;
    faSetting.init(), head.init(), nav.init(), tab.init();
    //向iframe注入方法
    tab.onload((w) => {
        //传递 nav.open事件
        w.router = function (opt) { nav.open(opt); }
        //监听主题的变化
        bodyCls.addListener(w);
    });
    //页面路由
    window.router = function (opt) { nav.open(opt); }
    let keys = [F.themeCls, F.animStatus, F.sideCls];
    //未开启侧栏状态保持、启动时清楚本地状态
    let holdSide = faSetting.get(F.holdSideStatus);
    if (!holdSide) { faSetting.set(F.sideCls, null) }
    keys.forEach((k) => {
        faSetting.onChange(k, true, (e, params) => { bodyCls.change(params); });
    });
    let pages = [{ title: "首页", url: "main.html", target: "TAB", lock: true }];
    pages.push(JSON.parse(faSetting.get(F.lastPage) || "{}"));
    let tmp = [];
    pages.forEach(p => {
        p.url ? (!tmp[p.url]) ? tmp.push(p.url) && router(p) : "" : "";
    })
    layui.layer.close(loading);
    //页面默认是隐藏状态、都加载完后再显示
    layui.jquery('.fa-layout').removeClass('layui-hide');
});
let s = [];
s.push('  ▁▁▁▁▁▁▁▁▁▁▁▁▁▁');
s.push(' |=====head====▏');
s.push(' |=n=▕▔▔▔▔▔▔▔▔▔▏');
s.push(' |=a=▕   TAB   ▏');
s.push(' |=v=▕▁▁▁▁▁▁▁▁▁▏');
console.log(s.join('\r\n'));
console.log('上图:页面结构&组件对应关系')