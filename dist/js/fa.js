/** 
* FishAdmin V1.0.0 
* By 小鱼
* https://gitee.com/hardyDou/fish-admin
 */

let Fa=function(){this.cache={};this.modules={};this.lisenter={};this.options={LOG_LEVEL:0,};}
const fa=new Fa();Fa.prototype.load=function(name,onLoad){if(this.cache[name]){onLoad(this.cache[name]);}
if(this.modules[name]){this.lisenter[name]=this.lisenter[name]||[];this.lisenter[name].push((mod)=>{onLoad(mod);});}}
Fa.prototype.set=function(name,app){this.cache[name]=app;(this.lisenter[name]||[]).forEach(onLoad=>onLoad(app));}
Fa.prototype.inner=function(name){this.modules[name]=true;}
Fa.prototype.unSupport=function(name){return!(this.cache[name]||this.modules[name]);}
Fa.prototype.use=function(deps,callback){if(typeof deps=="function"){callback=deps;deps=[];}
deps=(typeof deps==='string')?[deps]:deps;let layuiDeps=deps.filter(dep=>this.unSupport(dep));let faDeps=deps.filter(dep=>!this.unSupport(dep));let count=faDeps.length;faDeps.forEach(dep=>{this.load(dep,(mod)=>{layui.dep=mod;count--;})});let tmp=setInterval(()=>{if(count<=0){clearInterval(tmp);layui.use(layuiDeps,callback);}},5)}
Fa.prototype.define=function(deps,callback){if(typeof deps=='function'){callback=deps;deps=[];}
deps=(typeof deps==='string')?[deps]:deps;let layuiDeps=deps.filter((dep)=>this.unSupport(dep));let faDeps=deps.filter((dep)=>!this.unSupport(dep));this.use(faDeps,()=>{layui.define(layuiDeps,(factory)=>{callback((app,exports)=>{this.set(app,exports);factory(app,exports);});});});}
Fa.prototype.config=function(options){layui.each(options,(k)=>{this.options[k]=options[k]});layui.config(options);}