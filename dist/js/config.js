let workpath = getWorkPath();
fa.config({
    base: workpath + 'js/modules/',
    version: "002",
    debug: true,
    lockPwd: "123456",
    LOG_LEVEL: 4,
});
/** 字段枚举 */
const F = {
    /**主题 */
    themeCls: "thm",
    /**菜单展开合并样式 */
    navMoreCls: "nav_more_cls",
    /**是否开启页面动画 */
    animStatus: 'anim_status',
    /**是否保持侧边状态栏 */
    holdSideStatus: 'hold_side_status',
    /**侧边栏状态 */
    sideCls: 'side_cls',
    /**锁屏状态 */
    lockScreenStatus: 'lock_screen_status',
    /* 最近一次打开的页面 */
    lastPage: 'last_page',
};

function getWorkPath() {
    let workPath = "./";
    let me = "js/config.js";
    let scripts = document.scripts;
    for (let i = 0; i < scripts.length; i++) {
        let src = scripts[i].src;
        let n;
        if (n = src.match(me)) {
            let s = src.indexOf("//");
            s = src.indexOf("/", s + 2);
            workPath = src.substr(s).replace(me, '');
            break;
        }
    }
    return workPath;
}