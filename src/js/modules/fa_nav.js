; fa.define(['jquery', 'laytpl', 'fa_log', 'fa_util', 'fa_tab', 'fa_setting'], function (exports) {
    let $ = layui.$,
        fa_tab = layui.fa_tab,
        MOD_NAME = "fa_nav",
        faSetting = layui.fa_setting,
        log = layui.fa_log.getLog(MOD_NAME),
        util = layui.fa_util;
    let Fa_Nav = function () {
        this.options = {
            nav_box: "fa-nav-box",
            nav_cls: "fa-nav",
            dir_cls: "fa-nav-dir",
            item_cls: "fa-nav-item",
            item_d_cls: "fa-nav-itemed",
            menuTpl: "menuTpl",
            shrink: true,//点击菜单后、菜单自动收缩起来
            menuUrl: "data/menu.json",
            title: (d) => d.title,
            target: (d) => d.target,
            children: (d) => d.children,
            url: (d) => d.url,
            icon: (d) => d.icon,
            lock: (d) => false,
        };
    }
    Fa_Nav.prototype.init = function (options) {
        util.extend(this.options, options || {});
        this.initUI();
    }

    Fa_Nav.prototype.initEvent = function () {
        let _this = this,
            o = _this.options,
            box = $("." + o.nav_cls);
        //初始化、监听相关配置
        faSetting.onChange(F.navMoreCls, true, (event, params) => {
            util.toggleClass($("." + this.options.nav_box)[0], params.oldVal, params.val)
        });
        box.children("." + o.dir_cls).on('click', function (event) {
            let self = $(this),
                isOn = self.hasClass(_this.options.item_d_cls);
            if (isOn) {
                _this.closeDir(self);
            } else {
                _this.options.shrink && _this.clearDir();
                _this.openDir(self)
            }
            event.stopPropagation();
            return false;
        })
        box.children("." + o.item_cls).on('click', function (event) {
            _this.options.shrink && _this.clearDir();
            _this.openItem($(this));
            event.stopPropagation();
            return false;
        })
        log.info("init event")
    }
    Fa_Nav.prototype.clearItem = function () {
        $("." + this.options.item_cls).removeClass(this.options.item_d_cls);
    }
    Fa_Nav.prototype.clearDir = function () {
        $("." + this.options.dir_cls).removeClass(this.options.item_d_cls);
    }
    Fa_Nav.prototype.myDir = function (ele) {
        //1级菜单没有dir
        if (!ele.parent().hasClass('fa-nav-box')) {
            return ele.parent().prev();
        }
        return false;
    }
    Fa_Nav.prototype.hasDir = function (ele) {
        return ele.next().children("." + this.options.dir_cls)
    }
    Fa_Nav.prototype.itemed = function (ele) {
        ele.addClass(this.options.item_d_cls);
        let mydir = this.myDir(ele);
        mydir && this.openDir(mydir);
    }
    Fa_Nav.prototype.openDir = function (ele) {
        let mydir = this.myDir(ele);
        ele.addClass(this.options.item_d_cls);
        mydir && this.openDir(mydir);
    }
    Fa_Nav.prototype.closeDir = function (ele) {
        let hasdir = this.hasDir(ele)
        log.info('hasdir', hasdir)
        ele.removeClass(this.options.item_d_cls);
        hasdir && hasdir.removeClass(this.options.item_d_cls);
    }
    Fa_Nav.prototype.openUrl = function (opt) {
        let url = opt.url, eleStor = '.' + this.options.item_cls + '[fa-href="' + url + '"]';
        let ele = $(eleStor);
        if (ele.length > 0) {
            this.openItem(ele);
        } else {
            this.open(opt);
        }
    }
    Fa_Nav.prototype.openItem = function (ele) {
        this.clearItem();
        this.itemed(ele);
        let opt = {
            title: ele.attr('title'),
            url: ele.attr('fa-href'),
            target: ele.attr('fa-target'),
            lock: ele.attr('fa-lock')
        }
        this.open(opt);
    }
    Fa_Nav.prototype.open = function (opt) {
        if (opt.target.toUpperCase() === "_BLANK") {
            window.open(util.getPath(opt.url));
            return;
        }
        if (opt.target.toUpperCase() === "JS") {
            eval(opt.url);
            return;
        }
        fa_tab.open(opt.title, util.getPath(opt.url), opt.lock || false);
    }
    Fa_Nav.prototype.initUI = function () {
        if (typeof this.options.reqData == 'function') {
            this.options.reqData((data) => {
                $("." + this.options.nav_box).html(this.build(data));
                this.initEvent();
            });
        } else {
            util.get(this.options.menuUrl, {}, (resp) => {
                $("." + this.options.nav_box).html(this.build(resp));
                this.initEvent();
            });
        }

    }

    Fa_Nav.prototype.build = function (data) {
        let result = [], children = this.options.children;

        data.forEach(((item) => {
            if (children(item) && children(item).length > 0) {
                result.push(this.buildDir(item));
            } else {
                result.push(this.buildItem(item));
            }
        }));
        return result.join('');
    }
    Fa_Nav.prototype.buildDir = function (d) {
        let title = this.options.title,
            icon = this.options.icon,
            children = this.options.children;
        return [
            '<li class="fa-nav-dir">',
            '   <em  class= "', icon(d), '" ></em>',
            '   <span>', title(d), '</span>',
            '</li >',
            '<ul class="fa-nav">',
            this.build(children(d)),
            '</ul>'].join('');
    }

    Fa_Nav.prototype.buildItem = function (d) {
        let title = this.options.title,
            icon = this.options.icon,
            url = this.options.url,
            target = this.options.target,
            lock = this.options.lock;
        let r = [];
        r.push('<li class="fa-nav-item" title="', title(d), '" fa-href="', url(d), '" fa-target="', target(d), '" fa-lock="', lock(d), '">')
        r.push('<em class="', icon(d), '"></em>');
        r.push('  <span>', title(d), '</span>');
        r.push('</li>');
        return r.join('');
    }

    exports(MOD_NAME, {
        nav: new Fa_Nav(),
        init: function (options) {
            this.nav.init(options);
        },

        open: function (opt = { url: "", target: "_blank", title: "title",lock:false }) {
            this.nav.openUrl(opt);
        }
    })
})