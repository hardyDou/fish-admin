; fa.define(['fa_log', 'fa_drawer', 'jquery', 'fa_event', 'form'], function (exports) {
    'use strict';
    let MOD_NAME = "fa_setting",
        $ = layui.jquery,
        form = layui.form,
        log = layui.fa_log.getLog(MOD_NAME),
        drawer = layui.fa_drawer,
        FaEvent = layui.fa_event;

    let Fa_Setting = function () {
    };
    Fa_Setting.prototype.init = function () {
        this.initEvent();
    }
    Fa_Setting.prototype.show = function () {
        drawer.render({
            offset: 'r',
            width: "300px",
            top: 50, bottom: 40,
            content: $("#setting").html()
        });
        let thmList = $(".fa-ctl-theme");
        //主题UI初始化、事件
        thmList.children("[alt='" + this.get(F.themeCls) + "']").addClass('on');
        thmList.children("img").on('click', (e) => {
            thmList.children(".on").removeClass('on');
            let thm = $(e.currentTarget).addClass('on').attr('alt');
            this.set(F.themeCls, thm);
        });
        //菜单展开箭头UI初始化、事件
        form.on('radio(navMoreCls)', (data) => {
            let navMoreCls = data.value;
            this.set(F.navMoreCls, navMoreCls);
        });
        form.val('setting', {
            'navMoreCls': this.get(F.navMoreCls)
        });

        //动画UI、事件
        form.on('checkbox(animStatus)', (data) => {
            if (data.elem.checked) {
                this.set(F.animStatus, data.value);
            } else {
                this.set(F.animStatus, "");
            }
        })
        form.on('checkbox(holdSideStatus)', (data) => {
            this.set(F.holdSideStatus, data.elem.checked);
        })
        form.val('setting', {
            'animStatus': this.get(F.animStatus) && true,
            'holdSideStatus': this.get(F.holdSideStatus) && true,
        });
    }

    /**
     * 初始化事件
     * 1、打开关闭设置页面事件:setting
     */
    Fa_Setting.prototype.initEvent = function () {
        //绑定所有模块(*)的setting事件
        FaEvent.onEvent("*", 'setting', (event, params) => {
            this.show();
        });
    }

    /**
     * 保存到stroage中
     * 
     * @param {*} key 
     * @param {*} val 
     */
    Fa_Setting.prototype.set = function (key, val) {
        let oldVal = this.get(key);
        layui.data(MOD_NAME, { key: key, value: val });
        this.notice(key, val, oldVal);
    }
    /**
     * 从 stroage取出值
     * 
     * @param {*} key 
     * @returns 
     */
    Fa_Setting.prototype.get = function (key) {
        let setting = layui.data(MOD_NAME);
        if (key) {
            return setting && setting[key];
        }
        return setting;
    }
    /**
     * 监听key的值变化
     * 
     * @param {*} key 
     * @param {*} init 
     * @param {*} func 
     */
    Fa_Setting.prototype.onChange = function (key, init, func) {
        FaEvent.onEvent(MOD_NAME, 'change_' + key, func);
        let val = this.get(key);
        if (init && val) {
            func.call(this, { mod: MOD_NAME }, { key: key, oldVal: "", val: val });
        }
    }
    /**
     * 值改变时通知监听者
     * 
     * @param {*} key 
     * @param {*} val 
     * @param {*} oldVal 
     */
    Fa_Setting.prototype.notice = function (key, val, oldVal) {
        FaEvent.event(MOD_NAME, 'change_' + key, { key: key, oldVal: oldVal, val: val });
    }

    exports(MOD_NAME, {
        obj: new Fa_Setting(),

        init: function () {
            this.obj.init();
        },
        get: function (key) {
            return this.obj.get(key);
        },
        set: function (key, val) {
            this.obj.set(key, val);
        },
        onChange: function (key, init, func) {
            this.obj.onChange(key, init, func);
        }

    });
});