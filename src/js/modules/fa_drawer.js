fa.define(['jquery', 'layer'], function (exports) {
    var $ = layui.$;
    var layer = layui.layer;
    //插件名称
    var mod_name = "fa_drawer";
    //外部接口
    var obj = {
        render: (i) => render(i),
    };
    var render = function (e) {
        if (e.offset == "r") {
            e.skin = 'layui-anim layui-anim-rl layui-layer-drawer';
            if (!e.area)
                e.area = e.width ? [e.width, '100%'] : ['300px', '100%'];
        }
        else if (e.offset == "l") {
            e.skin = 'layui-anim layui-anim-lr layui-layer-drawer';
            if (!e.area)
                e.area = e.width ? [e.width, '100%'] : ['300px', '100%'];
        }
        else if (e.offset == "t") {
            e.skin = 'layui-anim layui-anim-down';
            if (!e.area)
                e.area = e.height ? ['100%', e.height] : ['100%', '300px'];
        }
        else if (e.offset == "b") {
            e.skin = 'layui-anim layui-anim-up';
            if (!e.area)
                e.area = e.height ? ['100%', e.height] : ['100%', '300px'];
        }

        var success = e.success;
        e.success = function (layero, index) {
            if (e.top != undefined)
                $(layero).css({ top: e.top });
            if (e.bottom != undefined)
                $(layero).css({ bottom: e.bottom });
            success && success();
        }
        var end = e.end;
        e.end = function () {
            layer.closeAll("tips");
            end && end();
        };
        
        layer.open($.extend({
            type: 1,
            id: 'fa-drawer',
            anim: -1,
            shade: 0.1,
            area: "336px",
            btnAlign: 'c',
            offset: 'r',
            skin: '',
            content: '空',
            title: false,
            move: false,
            closeBtn: false,
            isOutAnim: false,
            shadeClose: true,
        }, e));
    };

    exports(mod_name, obj);
});