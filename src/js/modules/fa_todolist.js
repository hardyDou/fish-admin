;fa.define(['fa_util', 'jquery', 'fa_event'], function (exports) {
    'use strict';
    let MOD_NAME = "fa_todolist", util = layui.fa_util,
        $ = layui.jquery,
        FaEvent = layui.fa_event,
        log = layui.fa_log.getLog(MOD_NAME);
    let Fa_ToDoList = function () {
        this.init();
    };
    Fa_ToDoList.prototype.init = function () {
        
    }


    exports(MOD_NAME, {
        o: new Fa_ToDoList(),
    })

});