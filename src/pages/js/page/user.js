layui.use('table', function () {
    var table = layui.table;
    //第一个实例
    table.render({
        elem: '#data'
        , toolbar: '#toolbarDemo'
        , totalRow: false
        , url: './data/user.json' //数据接口
        , where: { stime: '', etime: '', username: '', name: '', orderBy: 'ctime desc' }
        , page: true //开启分页
        , response: {
            statusCode: 0
        }
        , cols: [[
            { checkbox: true, type: 'checkbox', fixed: 'left' }
            , { field: 'userId', title: 'ID', width: 80, sort: true, fixed: 'left' }
            , { field: 'username', title: '用户名', width: 120, }
            , { field: 'name', title: '姓名', width: 100, }
            , { field: 'sex', title: '性别', templet: '#sexTml', width: 80 }
            , { field: 'mobile', title: '手机号', width: 120 }
            , { field: 'email', title: '邮箱', minWidth: 150 }
            // , {field: 'avatar', title: '头像', width: 80}
            , { field: 'status', title: '状态', templet: '#statusTpl', width: 100 }
            , { field: 'ctime', title: '创建时间', width: 180 }
            , { title: '操作', align: 'center', templet: '#barTpl', minWidth: 350 }
        ]],
        parseData: function (res) {
            return {
                code: res.code,
                msg: res.message,
                count: res.data.total,
                data: res.data.list,
                totalRow: {
                    score: "score",
                    experience: "exp"
                }
            }

        }
        , request: {
            pageName: 'pageNum' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
    });

});