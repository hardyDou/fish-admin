const gulp = require('gulp');
const header = require('gulp-header');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
const jsmin = require('gulp-jsmin');
var pkg = require("./package.json");
const include = require('gulp-include')
var less = require("gulp-less");
var connect = require("gulp-connect");
var inner = require("./src/js/fa-inner.js");
const browserSync = require('browser-sync');

let banner = "/** \n\
* FishAdmin V" + pkg.version + " \n\
* By 小鱼\n\
* https://gitee.com/hardyDou/fish-admin\n \
*/\n";


function minJs() {
    return gulp.src(['./src/js/**/*.js'], { sourcemaps: true })
        .pipe(jsmin())
        .pipe(header(banner))
        .pipe(gulp.dest('dist/js'))

}


function css() {
    return gulp.src('src/less/**/*.less')
        .pipe(less())
        .pipe(concat('fa.css'))
        .pipe(header(banner))
        .pipe(gulp.dest('dist/css'));
}


function alljs() {
    return gulp.src(['src/js/{fa,fa-inner}.js', 'src/js/modules/*.js'], { sourcemaps: true })
        .pipe(concat('fa.all.js'))
        .pipe(jsmin())
        .pipe(header(banner))
        .pipe(gulp.dest('dist/js'))
}


function pages() {
    return gulp.src(['src/pages/**',
        '!./**/_*.html'])
        .pipe(include())
        .pipe(gulp.dest('dist'))
}
function images() {
    return gulp.src(['src/images/**'])
        .pipe(gulp.dest('dist/images'))
}

function libs() {
    return gulp.src('src/libs/**').pipe(gulp.dest('dist/libs'))
}
gulp.task('images', images);//图片
gulp.task('css', css);//压缩css
gulp.task('minJs', minJs);//压缩所有js
gulp.task('alljs', alljs);//打包一个js
gulp.task('pages', pages);//移动页面资源
gulp.task('libs', libs);//依赖库

gulp.task('watch', function () {
    gulp.watch('src/images/**', gulp.parallel('images'));
    gulp.watch('src/js/**/*.js', gulp.series('minJs'));
    gulp.watch('src/lib/**', gulp.parallel('libs'));
    gulp.watch('src/less/**/*.less', gulp.parallel('css'));
    gulp.watch('src/pages/**', gulp.parallel('pages'));
});

gulp.task('server', function () {
    browserSync.init({
        server: "./dist",
        port: 8089
    })
    gulp.watch("./dist/**/*.*").on('change', browserSync.reload);
});

gulp.task("default", gulp.parallel('watch', 'server'));
gulp.task('build', gulp.parallel("css", "minJs", "alljs", "images", "pages", "libs"));
